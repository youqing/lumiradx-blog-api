**Overview**

This is function testing for LumiraDXBlog API using Python Unit Test.
The test is written using Python 3.7.


**How to Run**

In the top-level directory:
    $ python tests/testLumiraDXBlogAPI.py
    
**Structure**

tests/testLumiraDXBlogAPI.py      Implement positive and negative testcases for GET /convert
tests/TestResults__main__.GetConvertAPITest_2020-mm-dd-hh-mm-ss.html    Html format test report will be auto generate here

**Gitlab**

The configuration for the gitlab stores in .gitlab-ci.yml,

which will use python 3.7 from the image in docker and in the before_script, I have include all the environment setup work for the scripts, will install all the packages from requirement.txt. After that, it will run GetConvertAPITest.py and will automatic trigger a build for each push.

Jobs could be found in CI / CD Jobs.

**TestCase Design**


Test cases design implement in Test cases design for LumiraDX Blog APIs.doc


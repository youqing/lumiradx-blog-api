# Online Python compiler (interpreter) to run Python online.
# Write Python 3 code in this online editor and run it.

import unittest
import requests
import HtmlTestRunner
import logging
import random


log = logging.getLogger(__name__)

positive_data = [
    ("Late that night, Moon-Watcher suddenly awoke. Tired out by the day’s exertions and disasters", "The New Time"),
    ("You could type anything here for testing", "Some Interesting Title")
]

negative_data = [
    (123, 12),
    ("NULL", "NULL")
]

class LumiraDXBlogAPITest(unittest.TestCase):
    def setUp(self):
        """
        Test Suite Setup
        :return:
        """
        self.base_url = ""
        self.headers = {''}
        self.mock_url = ""

    @unittest.skip("Skip this test for now")
    def test_get_list_of_blog_post(self):
        """
        Send request to GET /blog/post, for success call
        :return:
        list of available blogs titles
        """
        available_blogs = []
        response = requests.get(self.base_url, headers=self.headers)
        log.debug(response.text)
        if self.response.status_code == 200:
            blogs = response.json()['blogs']
            for item in blogs:
                title = item["title"]
                available_blogs.append(title)
            return available_blogs

    def post_blog_succeed(self):
        """
        Loop through the data in list of positive_data and use these data to generate endpoint of the url and
        send request to POST / blog/posts
        :return:
            response
        """
        for body, title in positive_data:
            url = self.base_url
            log.debug(url)
            response = requests.post(self.url, headers=self.headers)
            # response = requests.get(self.mock_url)
            log.debug(response.text)
        return response

    def test_post_blog_succeed(self):
        """
        Loop through the data in list of positive_data and use these data to validation response status code and response body
        :return:
            NULL
        """
        for body, title in positive_data:
            self.assertEqual(201, self.post_blog_succeed().status_code)
            self.assertEqual(body, self.get_blog_succeed().json()['body'])
            self.assertEqual(title, self.get_blog_succeed().json()['title'])
            # TODO: Here maybe to do the enhancement to grab the expected value to category, category_id, id and pub_date.


    def post_blog_failed(self):
        """
        Loop through the data in list of negative_data and use these data to generate endpoint of the url and
        :return:
            response
        """
        for body, title in negative_data:
            url = self.base_url
            response = requests.get(url, headers=self.headers)
            log.debug(response.text)
            return response

    def test_get_convert_failed(self):
        """"
        Loop through the data in list of negative_data and use these data to validation response status code and response body
        :return:
            NULL
        """
        for body, title in negative_data:
            self.assertEqual(422, self.post_blog_failed().status_code)
        # TODO: More validation could be add here base on response body


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    unittest.main(testRunner = HtmlTestRunner.HTMLTestRunner(output='.'))